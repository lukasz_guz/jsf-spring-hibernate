package pl.jsf;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import pl.jsf.dao.PersonDao;
import pl.jsf.entity.Person;

public class Main {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        
		PersonDao personDao = context.getBean(PersonDao.class);
         
        Person person = new Person();
        person.setFirstName("�ukasz");
        person.setLastName("Guz");
        
        personDao.save(person);
        System.out.println("Person::"+person);
        
        Person person2 = personDao.find(person.getId());
        System.out.println("Person::"+person2);
        
        List<Person> list = personDao.findAll();
        for (Person p : list) {
        	System.out.println("Person::"+p);
		}
         
        context.close();    
	}
}